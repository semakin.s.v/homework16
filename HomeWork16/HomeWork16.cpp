#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctime>

int main()
{
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }

    time_t now = time(0);
    tm* ltm = localtime(&now);
    int day = ltm->tm_mday;

    int index = day % N;
    int sum = 0;
    for (int i = 0; i < N; i++)
    {
        sum += array[index][i];
    }

    std::cout << sum;



    

}
